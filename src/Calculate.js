
import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Form, Button } from 'react-bootstrap';

let index = 1;
class Calculate extends Component {
    constructor(props) {
        super(props)
        this.state = {
            num1: '',
            num2: '',
            cal: '+'
        }
    }

    handleChange = (e) => {
        this.setState(
            {
                [e.target.name]: e.target.value
            }
        )
    }

    onCalculate = (e) => {
        let result = 0;
        if (this.state.num1 === "") {
            alert("Please input number 1:");
            return;
        }
        else if (this.state.num2 === "") {
            alert("Please input number 2:");
            return;
        }
        
        e.preventDefault();
        const formData =
        {
            num1: this.state.num1,
            num2: this.state.num2,
            cal: this.state.cal
        }
        var number1 = parseInt(formData.num1);
        var number2 = parseInt(formData.num2);

        if (formData.cal === "+") {
            result = number1 + number2;
        }
        else if (formData.cal === "-") {
            result = number1 - number2;
        }
        else if (formData.cal === "*") {
            result = number1 * number2;
        }
        else if (formData.cal === "/") {
            result = number1 / number2;
        }
        else if (formData.cal === "%") {
            result = number1 % number2;
        }
        var Obj =
        {
            id: index++,
            mainResult: result
        }
        this.props.dataFromParent(Obj);
    }

    render() {
        return (

            <div>
                <Form>
                    <Form.Group>
                        <Form.Control type="number" name="num1" value={this.state.num1} onChange={e => this.handleChange(e)} placeholder="Please input number"></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Control type="number" name="num2" value={this.state.num2} onChange={e => this.handleChange(e)} placeholder="Please input number"></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Control as="select" name="cal" onChange={e => this.handleChange(e)} value={this.state.cal} className="control">
                            <option value="+">+ Addition</option>
                            <option value="-">- Subtraction</option>
                            <option value="*">* Multiple</option>
                            <option value="/">/ Devide</option>
                            <option vlaue="%">% Modulus</option>
                        </Form.Control>

                    </Form.Group>
                    <Button type="button" onClick={
                        (e) => this.onCalculate(e)
                    }>Calculate</Button>
                </Form>
            </div>

        )
    }

}

export default Calculate;















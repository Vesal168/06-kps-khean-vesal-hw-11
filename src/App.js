import React, { Component } from 'react';
import Calculate from './Calculate';
import Result from './Result';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card } from "react-bootstrap";
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resultVal: []
    }
  }

  handleResult(val) {
    this.setState({
      resultVal: [...this.state.resultVal,val]
    })
  }
  render() {
    return (
      <div>
        <div className="my-container">
          <div className="container header">
            <div className="row">
              <div className="col-md-6" id="img">
                <Card.Img className="img"
                  variant="top"
                  src="https://icons.iconarchive.com/icons/dtafalonso/android-lollipop/512/Calculator-icon.png"
                />
              </div>
              <div className="col-md-6" id="result">
                <Result results={this.state.resultVal} />
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-md-6" id="calculate">
                <Calculate dataFromParent={
                  this.handleResult.bind(this)
                } />
              </div>
            </div>
          </div>
        </div>

      </div>

    )

  }
}
export default App;


import React from 'react'
import { ListGroup } from 'react-bootstrap';
function Result(props) {
    let myResultNumbers = props.results;
    return (
        <div>
            <h3>Display Result</h3>
            <ul>
                {
                    myResultNumbers.map(val => {
                        return (
                            <ListGroup>
                                <ListGroup.Item key={val.id}>{val.mainResult}</ListGroup.Item>
                            </ListGroup>
                        );
                    })
                }
            </ul>
        </div>
    )
}
export default Result;